# A/C repeater

Program for *ESP8266*-based Lolin D1 Mini. Can record and repeat IR signal.

**Warning**: This project is still WIP. More info, docs and features to be added soon. Also, code might be not really pretty for now.

## Schematic

![Scheme](schematic.png)