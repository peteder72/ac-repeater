//let host = "http://192.168.4.1";
let host = "";
let configs_ep = host + "/configs"
let scan_ep = host + "/wifi/scan";

get_configs();

function populate_class(val, tag) {
    
    Array.from(document.getElementsByClassName(tag)).forEach(element => {
        element.innerHTML = val
    });

}

function remove_options(element) {
   var i, len = element.options.length - 1;
   for(i = len; i >= 0; i--) {
      element.remove(i);
   }
}

function scan() {
    console.log("Asking ESP to scan the network");
    let xhr = new XMLHttpRequest();
    xhr.open("GET", scan_ep, true);
    xhr.responseType = "json";
    xhr.onload = () => {
        if (xhr.status == 200) {
            var json = xhr.response;
            var input = document.getElementById("wifi_networks");

            remove_options(input);

            console.log("Got " + json.length + " networks");
            json.forEach(network => {
                input.appendChild(net_to_html(network));
            });
        } else {
            console.log("Could not get scan: code " + xhr.status + ". Try refreshing the page");
        }
    };
    xhr.send();
}

function net_to_html(network) {
    var text = network['ssid'] + " (" + network['security'] + ")"

    var opt = document.createElement('option');
    opt.appendChild(document.createTextNode(text));
    opt.value = network['ssid']; 

    return opt;
}

function format_ap(is_ap, ssid, esp_id) {
    if (is_ap) {
        return "Running in access point mode, ssid " + ssid.replace("%08X", esp_id.padStart(8, '0'));
    } else {
        return "Connected to WiFi " + ssid;
    }
}

function get_configs() {
    let xhr = new XMLHttpRequest();
    xhr.open("GET", configs_ep, true);
    xhr.responseType = "json";
    xhr.onload = () => {
        if (xhr.status == 200) {
            var json = xhr.response;
            var esp_id = json['id'].toUpperCase();

            populate_class(esp_id, 'esp_id');
            populate_class(json['ip'], 'ip_addr');
            populate_class('0x' + json['command'].toUpperCase(), 'current_command');
            populate_class(format_ap(json['is_ap'], json['ssid'], esp_id), "conn_status");
        } else {
            console.log("Could not get ESP configs: code " + xhr.status + ". Try refreshing the page");
        }
    };
    xhr.send();
}