#include "button.hpp"

Button::Button(int pin): pin(pin) {
    pinMode(pin, INPUT_PULLUP);
}

bool Button::pressed() {
    // Debouncing routine
  if (digitalRead(pin) != state)
  {
    if (timer == 0)
      timer = millis();

    if (millis() - timer > DBNC)
    {
      if (digitalRead(pin) != state)
      {
        state = !state;
        timer = 0;

        return !state;
      }
    }
  }

  return false;
}