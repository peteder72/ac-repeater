#include <Arduino.h>

#include <EEPROM.h>
#include <FS.h>

#include <ESP8266WebServer.h>

#include <IRremoteESP8266.h>
#include <IRsend.h>
#include <IRrecv.h>

#include "button.hpp"
#include "status.hpp"
#include "server.hpp"

#define BTN_PLAY D7
#define BTN_RECORD D3

#define RECV_PIN D5
#define SEND_PIN D2

#define ST_LED_PIN D0

#define ADDR_CMD 0
#define ADDR_CMD_LEN 4

#define WIFI_CREDS_FILENAME "wifi_creds"

const uint16_t kCaptureBufferSize = 1024;

IRrecv irrecv(RECV_PIN, kCaptureBufferSize);
IRsend irsend(SEND_PIN);

decode_results results;

bool local_ap;
unsigned long recorded_command = 0;

Button *btn_record, *btn_play;
Status *status;
ESP8266WebServer *serv;

void index_handler()
{
  File f = SPIFFS.open("/index.html", "r");
  Serial.println("Accessing index");
  serv->streamFile(f, "text/html");
  f.close();
}

void script_handler()
{
  File f = SPIFFS.open("/script.js", "r");
  Serial.println("Accessing js");
  serv->streamFile(f, "text/javascript");
  f.close();
}

void scan_handler() {
  int n = WiFi.scanNetworks();

  String json = scan_to_json(n);

  serv->send(200, "application/json", json);

}

void config_handler()
{

  WebConfig conf = WebConfig(
      (local_ap) ? WiFi.softAPIP() : WiFi.localIP(),
      ESP.getChipId(),
      recorded_command,
      (local_ap) ? DEFAULT_SSID : "real_ssid",
      (local_ap) ? String(DEFAULT_PASSWD) : String("real_password"),
      local_ap);

  Serial.println(conf.json());

  serv->send(200, "application/json", conf.json());
}

void setup()
{
  Serial.begin(9600);
  EEPROM.begin(512);
  SPIFFS.begin();

  local_ap = !SPIFFS.exists(WIFI_CREDS_FILENAME);

  btn_record = new Button(BTN_RECORD);
  btn_play = new Button(BTN_PLAY);

  status = new Status(ST_LED_PIN);

  serv = new ESP8266WebServer(80);

  pinMode(SEND_PIN, OUTPUT);

  for (int i = 0; i < ADDR_CMD_LEN; i++)
  {
    uint8_t val = EEPROM.read(ADDR_CMD + (ADDR_CMD_LEN - i - 1));
    Serial.print("Read ");
    Serial.print(val, HEX);
    Serial.print(" from ");
    Serial.println(ADDR_CMD + (ADDR_CMD_LEN - i - 1));
    recorded_command += val;
    if (i != ADDR_CMD_LEN - 1)
    {
      recorded_command <<= 8;
    }
  }

  if (recorded_command == 0xFFFFFFFFul || recorded_command == 0)
  {
    recorded_command = 0;
    status->recording(true);
    Serial.println("No saved command found");
  }
  else
  {
    status->recording(false);
    Serial.print("Loaded command value: ");
    Serial.println(recorded_command, HEX);
  }

  irrecv.enableIRIn();

  if (local_ap)
  {
    create_soft_ap();
  }

  serv->on("/", index_handler);
  serv->on("/script.js", script_handler);
  serv->on("/configs", config_handler);
  serv->on("/wifi/scan", scan_handler);

  serv->begin();
}

void eeprom_dump(uint32_t data, int addr, int len)
{
  for (int i = 0; i < len; i++)
  {
    uint8_t val = (data >> (i * 8ul)) & 0xFFul;
    EEPROM.write(addr + i, val);
  }
}

void loop()
{
  serv->handleClient();
  status->update();
  if (status->is_recording() && irrecv.decode(&results))
  {
    if (results.decode_type != NEC)
    {
      status->error();
    }
    else if (results.decode_type == NEC && results.value != 0xFFFFFFFFFFFFFFFFull)
    {
      recorded_command = results.value;
      Serial.print("Recorded ");
      Serial.print((uint32_t)(results.value), HEX);
      Serial.print(" ");
      Serial.println(results.bits);

      eeprom_dump(recorded_command, ADDR_CMD, ADDR_CMD_LEN);
      EEPROM.commit();
      Serial.println("Written EEPROM values");
      status->recording(false);
    }
    // dump(&results);
    irrecv.resume(); // Receive the next value
  }

  if (btn_play->pressed())
  {
    Serial.print("Sending ");
    status->playing(true);
    Serial.println(recorded_command, HEX);
    irsend.sendNEC(recorded_command, 32);
  }

  if (btn_record->pressed())
  {
    status->recording(true);
    Serial.println("Now recording");
  }
}