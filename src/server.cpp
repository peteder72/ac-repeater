#include "server.hpp"

void create_soft_ap()
{
    char ssid_buf[32];

    sprintf(ssid_buf, DEFAULT_SSID, ESP.getChipId());

    WiFi.softAP(ssid_buf, DEFAULT_PASSWD);

    Serial.print("Soft AP started on ");
    Serial.println(ssid_buf);
    Serial.println(WiFi.softAPIP());
}

String WebConfig::json()
{
    // Size value seems very random?
    DynamicJsonDocument j(2048);

    j["id"] = id;
    j["ip"] = ip;
    j["command"] = command;
    j["ssid"] = ssid;
    j["pwd_len"] = pwd_len;
    j["is_ap"] = is_ap;

    String res = String();

    serializeJson(j, res);
    return res;
}

WebConfig::WebConfig(
    IPAddress ip_raw,
    uint32_t chip_id_raw,
    uint32_t command_raw,
    String ssid_inp,
    String password,
    bool ap)
{
    ip = ip_raw.toString();
    id = String(chip_id_raw, HEX);
    command = String(command_raw, HEX);
    ssid = ssid_inp;
    pwd_len = password.length();
    is_ap = ap;
}

String scan_to_json(int len) {

    StaticJsonDocument<2048> j;
    JsonArray arr = j.to<JsonArray>();

    Serial.print("Found networks: ");
    Serial.println(len);

    for (int i = 0; i < len; i++) {
        String ssid = WiFi.SSID(i);
        String encryption_s;

        switch (WiFi.encryptionType(i)) {
            case ENC_TYPE_WEP:
                encryption_s = "WEP";
                break;
            case ENC_TYPE_TKIP:
                encryption_s = "WPA";
                break;
            case ENC_TYPE_CCMP:
                encryption_s = "WPA2";
                break;
            case ENC_TYPE_NONE:
                encryption_s = "Open";
                break;
            case ENC_TYPE_AUTO:
                encryption_s = "WPA/WPA2";
                break;
            default:
                encryption_s = "Unknown";
                break;
        }

        StaticJsonDocument<256> t;

        t["ssid"] = ssid;
        t["security"] = encryption_s;

        arr.add(t);
    }

    String res;

    serializeJson(arr, res);

    return res;
}