#pragma once

#include <ESP8266WiFi.h>
#include <ArduinoJson.h>
#include <ESP8266WebServer.h>

#define DEFAULT_SSID "ESP_%08X_WiFi"
#define DEFAULT_PASSWD "secret_flupke"

class WebConfig
{
    String ip;
    String id;
    String command;
    String ssid;
    bool is_ap;
    int pwd_len;

public:
    String json();
    WebConfig(
        IPAddress ip_raw,
        uint32_t chip_id_raw,
        uint32_t command_raw,
        String ssid,
        String password,
        bool ap);
};

void create_soft_ap();
String scan_to_json(int len);