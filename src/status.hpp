#pragma once

#include <Arduino.h>

#define BLINK_PULSE 500
#define PLAY_PULSE 200

#define RECORDED_PULSE 100
#define RECORDED_NTIMES 10

#define ERROR_ON_PULSE 500
#define ERROR_OFF_PULSE 100

enum State {
    IDLE,
    RECORDING,
    RECORDED,
    PLAYING,
    ERROR
};

class Status {

    State cur_state;
    uint32_t timer;
    int led_pin;
    bool led_state;
    int counter;

    void write(bool val);

    public:

    Status(int pin);
    void update();
    void recording(bool val);
    void playing(bool val);
    void error();

    bool is_recording();
};